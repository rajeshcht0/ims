<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$purchase = $_POST['data'];
	
	$sql = "INSERT INTO purchase (purchase_date, fk_supplier_id, grand_total, payment, due_amount, description, pay_mode, due_date)
			VALUES(
				'{$purchase['date']}',
				'{$purchase['supplier']}',
				'{$purchase['grand_total']}',
				'{$purchase['payment']}',
				'{$purchase['due_balance']}',
				'{$purchase['description']}',
				'{$purchase['pay_mode']}',
				'{$purchase['due_date']}'
			)";

			// id of purchase bill
			$purchase_id = NULL;

			if ($conn->query($sql) === TRUE) {
				$purchase_id = $conn->insert_id;
			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}

			// if purchase record is not added
			if(is_null($purchase_id)){
				// echo "Something went wrong.";
				echo json_encode("Something went wrong.");
				die();
			}
	$products = $_POST['data']['items'];

	foreach ($products as $product) {
		$sql = "INSERT INTO product (purchase_id, name, attribute_type, attribute, quantity, buy_rate, sales_rate, total)
				VALUES (
					'{$purchase_id}',
					'{$product['name']}',
					'{$product['attribute_type']}',
					'{$product['attribute']}',
					'{$product['quantity']}',
					'{$product['buy_rate']}',
					'{$product['sales_rate']}',
					'{$product['total']}'
				)";
		if ($conn->query($sql) === TRUE) {

		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}
	}

	echo json_encode("New record added successfully.");

	$conn->close();
