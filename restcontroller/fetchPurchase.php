<?php

	$product = $_GET['product'];
	$attribute_type = $_GET['attribute_type'];
	$attribute = $_GET['attribute'];

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$sql = "
		SELECT p.id, p.product_name, pp.purchase_id, pp.attribute_type, pp.attribute, pp.quantity, pp.sales_rate
		FROM product p
		LEFT JOIN product_purchase pp ON p.id = pp.product_id
		WHERE pp.product_id = '{$product}' AND pp.attribute_type = '{$attribute_type}' AND pp.attribute = '{$attribute}' AND pp.quantity!=0
	";

	$result = $conn->query($sql);
	$result_array = [];
	while ( $row = $result->fetch_assoc()) {
		array_push($result_array, $row);
	}

	echo json_encode($result_array);

	$conn->close();
