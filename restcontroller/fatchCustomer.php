<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$sql = "SELECT id, cus_name FROM customer";
	$result = $conn->query($sql);

	$result_array = [];
	while ( $row = $result->fetch_assoc()) {
		array_push($result_array, $row);
	}

	echo json_encode($result_array);

	$conn->close();