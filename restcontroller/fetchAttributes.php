<?php
	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

	$attribute_type = $_GET['attr_type'];
	$product_id = $_GET['prod_id'];

	$sql = "
		SELECT DISTINCT attribute FROM product_purchase
		WHERE product_id = {$product_id} AND attribute_type = '{$attribute_type}'
	";

	$result = $conn->query($sql);
	
	$result_array = [];
	while ( $row = $result->fetch_assoc()) {
		array_push($result_array, $row);
	}
	echo json_encode(["code"=> "200", "data"=> $result_array]);
	$conn->close();