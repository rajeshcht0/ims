<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}

	$purchase = $_POST['data'];

	$sql = "INSERT INTO purchase (billno, purchase_date, supplier, total_amount, paid_amount, balance_amount, description, pay_mode, due_date)
			VALUES(
            '{$purchase['bill_number']}',
            '{$purchase['date']}',
            '{$purchase['supplier']}',
            '{$purchase['grand_total']}',
            '{$purchase['payment']}',
            '{$purchase['due_balance']}',
            '{$purchase['description']}',
            '{$purchase['pay_mode']}',
            '{$purchase['due_date']}'
			)";

	$master_bill_id = NULL;
	if ($conn->query($sql) === TRUE) {
	    $master_bill_id = $conn->insert_id;
	} else {
		$error = "Error: " . $sql . " message: " . $conn->error;
	    echo json_encode($error);
	}

	$products = $_POST['data']['items'];


	foreach ($products as $product) {
		$sql1 = "INSERT INTO product_purchase (product_id, purchase_id, attribute_type, attribute, quantity, buy_rate, sales_rate)
				VALUES (
					'{$product['product_id']}',
					'{$master_bill_id}',
					'{$product['attribute_type']}',
					'{$product['attribute']}',
					'{$product['quantity']}',
					'{$product['buy_rate']}',
					'{$product['sales_rate']}'
				)";

		if ($conn->query($sql1) === TRUE) {
        }
        else {
		    echo "Error: " . $sql1 . "<br>" . $conn->error;
		}
		$sql2 = "INSERT INTO product_purchase_record (product_id, purchase_id, attribute_type, attribute, quantity, buy_rate, sales_rate)
				VALUES (
					'{$product['product_id']}',
					'{$master_bill_id}',
					'{$product['attribute_type']}',
					'{$product['attribute']}',
					'{$product['quantity']}',
					'{$product['buy_rate']}',
					'{$product['sales_rate']}'
				)";

		if ($conn->query($sql2) === TRUE) {
				}
				else {
				echo "Error: " . $sql1 . "<br>" . $conn->error;
		}
	}

	echo json_encode("New record added successfully.");

	$conn->close();
