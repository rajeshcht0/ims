<?php

	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "ims";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	}
	$product_id = $_GET['id'];
	$sql = "SELECT DISTINCT attribute_type from product_purchase WHERE product_id = {$product_id}";

	$result = $conn->query($sql);

	$result_array = [];

	if($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			if($row != '') {
				array_push($result_array, $row);
			}
		}
	}

	if(empty($result_array)){
		echo json_encode(["code"=> "404", "message"=> "No record found."]);
		die();
	}

	echo json_encode(["code"=> "200", "data"=> $result_array]);
	die();
	