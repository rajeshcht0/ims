<?php

session_start();
$base_url = 'http://localhost/project';
$_SESSION['base_url'] = $base_url;
$_SESSION['active_url'] = '';
include 'Helper/SpecialCharHelper.php';
include 'Helper/FlashMessageHelper.php';
include 'Helper/ErrorHelper.php';
include 'Helper/RouteHelper.php';


if (isset($_GET['r'])) {
    $controller = $_GET['r'];
    switch ($controller) {
        case 'welcome':
            $_SESSION['active_url'] = 'home';
            include 'controller/WelcomeController.php';
            break;
        case 'about':
            $_SESSION['active_url'] = 'login';
            include 'controller/AboutController.php';
            break;
        case 'contact':
            $_SESSION['active_url'] = 'signup';
            include 'controller/ContactController.php';
            break;
        case 'login':
            include 'controller/LoginController.php';
            break;
        case 'dashboard':
            $_SESSION['active_url'] = 'note-list';
            include 'controller/DashboardController.php';
            break;
        case 'addcustomer':
            $_SESSION['active_url'] = 'new-note';
            include 'controller/AddcustomerController.php';
            break;
        case 'addsuppliers':
            $_SESSION['active_url'] = 'view-note';
            include 'controller/AddsuppliersController.php';
            break;
        case 'adduser':
            $_SESSION['active_url'] = 'update-note';
            include 'controller/AdduserController.php';
            break;
        case 'edituser':
            $_SESSION['active_url'] = 'delete-note';
            include 'controller/EdituserController.php';
            break;
        case 'viewuser':
            $_SESSION['active_url'] = 'delete-note';
            include 'controller/ViewuserController.php';
            break;
        case 'services':
            $_SESSION['active_url'] = 'delete-note';
            include 'controller/ServicesController.php';
            break;
        case 'userprofile':
            $_SESSION['active_url'] = 'delete-note';
            include 'controller/UserprofileController.php';
            break;
        case 'addproduct':
             $_SESSION['active_url'] = 'delete-note';
            include 'controller/AddProductController.php';
            break;
        case 'logout':
             $_SESSION['active_url'] = 'delete-note';
            include 'controller/LogOutController.php';
            break;
        case 'purchase':
             $_SESSION['active_url'] = 'delete-note';
            include 'controller/PurchaseController.php';
            break;
        case 'sales':
             $_SESSION['active_url'] = 'delete-note';
            include 'controller/SalesController.php';
            break;
        case 'viewcustomer':
                 $_SESSION['active_url'] = 'delete-note';
                include 'controller/ViewCustomerController.php';
                break;
        case 'viewsuppliers':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/ViewSuppliersController.php';
                        break;
            case 'report':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/ViewReportController.php';
                        break;
            case 'viewpurchase':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/ViewPurchaseController.php';
                        break;
            case 'stock':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/StockController.php';
                        break;
            case 'pending':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/PendingController.php';
                        break;
            case 'updatesuppliers':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/UpdatesuppliersController.php';
                        break;
            case 'updatecustomer':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/UpdateCustomerController.php';
                        break;
            case 'viewproduct':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'controller/ViewProjectController.php';
                        break;
            case 'test':
                         $_SESSION['active_url'] = 'delete-note';
                        include 'view/test.php';
                        break;
            case 'viewsales':
                         $_SESSION['active_url'] = 'delete-note';
                          include 'controller/ViewSalesController.php';
                          break;
            case 'outstanding':
                          $_SESSION['active_url'] = 'delete-note';
                            include 'controller/OutstandingController.php';
                              break;
          case 'payoutstanding':
                            $_SESSION['active_url'] = 'delete-note';
                            include 'controller/PayOutstandingController.php';
                            break;
          case 'paypending':
                            $_SESSION['active_url'] = 'delete-note';
                            include 'controller/PayPandingController.php';
                            break;
          case 'payment':
                            $_SESSION['active_url'] = 'delete-note';
                            include 'controller/PaymentController.php';
                            break;

        default :
            throwError(404, 'Requested page does not exists.');
            break;
    }
    return;
} else {
    include 'controller/WelcomeController.php';
}
