<?php
 $connection =mysqli_connect('localhost', 'root', '', 'ims');
 if(!$connection) {
         die("database connection failed");
       }
//else {
//    echo "connect";
//}
//$cat, $model, $modelno, $brand, $manu, $productname,$productdis,$warrentype,$warrentyterms
/*function my_contact_list($user_id) {
    $conn = db_connect();
    $sql = "SELECT contact.id as contact_id,name,address,email,number,create_date FROM contact LEFT JOIN phone ON phone.fk_contact_id=contact.id where fk_user_id=" . $user_id . " order by contact.id";
    $result = $conn->query($sql);
    $conn->close();
    if ($result->num_rows > 0) {
        return $result;
    } else {
        return false;
    }
}*/
function db_connect() {
    $db['host'] = "localhost";
    $db['username'] = "root";
    $db['password'] = "";
    $db['db_name'] = "ims";

    $conn =mysqli_connect($db['host'], $db['username'], $db['password'], $db['db_name']);

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

//$cat, $model, $modelno, $brand, $manu, $productname,$productdis,$warrentyperiod,$warrentyterms
function addproduct($cat, $model, $modelno, $brand, $manu, $productname,$productdis,$warrentyperiod,$warrentyterms) {
    $conn = db_connect();

    $conn->begin_transaction();
    $stmt = $conn->prepare("INSERT INTO product (product_name,product_description,warranty_period,warranty_terms) values(?, ?, ?, ?)");
    $stmt->bind_param('ssss', $productname, $productdis, $warrentyperiod, $warrentyterms);
    $flag = TRUE;
    $last_id = NULL;
    if ($stmt->execute()) {
        $last_id = $conn->insert_id;

            $stmt2 = $conn->prepare("INSERT INTO product_item_category (category_name,fk_product_id) values(?, ?)");
            $stmt2->bind_param('si', $cat, $last_id);
        
            $stmt3 = $conn->prepare("INSERT INTO product_item_model (model_name,model_no,fk_product_id) values(?, ?, ?)");
            $stmt3->bind_param('ssi', $model, $modelno, $last_id);
        
            $stmt4 = $conn->prepare("INSERT INTO product_brand (manifacturer,brand_name,fk_product_id) values(?, ?, ?)");
            $stmt4->bind_param('ssi', $manu, $brand, $last_id);
       
            if (!($stmt3->execute() and $stmt2->execute() and $stmt4->execute())) {
                $flag = FALSE;
            }
        
    } else {
        $flag = FALSE;

    }
    if ($flag) {
        $conn->commit();
        return $last_id;
        
    } else {
       $conn->rollback();
        $stmt->close();
        $conn->close();
        return false;

    }
}
     
 



?>