<?php
include 'model/db.php';


if (empty($_POST)) {
    include 'view/addproduct.php';
    return;
}
try {
    $flag = empty($_POST['category']) || empty($_POST['brand']) || empty($_POST['manufacturer']) || empty($_POST['productname']);

    //validate user inputdata
    if ($flag) {
        $error['body'] = 'All input field are required.';
        $error['title'] = 'Danger!!';
        $error['type'] = 'danger';
        setFlash('message', $error);
        include 'view/signup.php';
        return;
    }
    $cat = filterText($_POST['category']);
    $model = filterText($_POST['model']);
    $modelno = filterText($_POST['modelno']);
    $brand = filterText($_POST['brand']);
    $manu = filterText($_POST['manufacturer']);
    $productname = filterText($_POST['productname']);
    $productdis = filterText($_POST['productdiscription']);
    $warrentperiod = filterText($_POST['warrentyperiod']);
    $warrentyterms = filterText($_POST['warrentyterms']);
    $addproduct = addproduct($cat, $model, $modelno, $brand, $manu, $productname, $productdis, $warrentperiod, $warrentyterms);
    if ($addproduct) {
        $msg['title'] = 'Success!!';
        $msg['body'] = "Product Added Successfully.";
        $msg['type'] = 'success';
        setFlash('message', $msg);
        redirect("login");
    } else {
        throwError(500, 'Unable to complete your request.');
    }
} catch (Exception $ex) {
    throwError();
}