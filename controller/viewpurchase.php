<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>


<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>


                    </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Purchase List</b></h3>
  </div>
  <div class="card-block">




    <div class="container" style="margin-top:35px">

        <table id="mytable" class="table table-bordered table-striped">

     <thead>
            <tr>

                <th>purchase_date</th>
                <th>billno</th>
                <th>supplier ID</th>
                <th>product ID</th>
                 <th>quantity</th>
                 <th>amount</th>
                <th>pay mode</th>
                 <th>Remark</th>
                 <th>Action</th>

            </tr>
        </thead>


        <tbody>
            <?php ;
$sel ="SELECT purchase.billno,purchase.purchase_date,purchase.id,product_purchase_record.product_id,purchase.supplier,purchase.pay_mode,purchase.total_amount,product_purchase_record.quantity,product_purchase_record.attribute_type,product_purchase_record.attribute FROM purchase LEFT JOIN product_purchase_record ON purchase.id=product_purchase_record.purchase_id
";
$res =mysqli_query($connection,$sel);
     while($val=mysqli_fetch_array($res,MYSQLI_ASSOC)){
         ?>
       <tr>

         <td><?php echo $val['purchase_date']; ?></td>
         <td><?php echo $val['billno']; ?></td>
         <td><?php echo $val['supplier']; ?></td>
         <td><?php echo $val['product_id']; ?></td>
        <td><?php echo $val['quantity']; ?></td>
         <td><?php echo $val['total_amount']; ?></td>
         <td><?php echo $val['pay_mode']; ?></td>
         <td><?php echo $val['attribute_type']; ?> : <?php echo $val['attribute']; ?></td>

        <td > <a href=edituser.php?id=<?php echo $val['id']; ?>><i class="fa fa-fw fa-pencil"></i></a> <a href=delete.php?id=<?php echo $val['id']; ?>&table=user&return=viewuser.php><i class="fa fa-fw fa-trash"></i></a></td>

         <?php  ;} ?>






 <!-- //  echo "<tr><td>1</td><td>$val[user_id]</td><td>$val[user_name]</td><td>$val[user_address]</td><td>$val[user_contact1]</td><td><a href='delete.php?user_id=$val[user_id]'>Delete</td>
<td><a href='update.php?user_id=$val[user_id]'>update</td></tr>";-->

 </tbody>
        </table>

    </div>

        <script>
    $(document).ready(function() {
    $('#mytable').DataTable();
} ); </script>
<script src="resource\DataTables_3\dataTables.min.js"></script>

    </div>


</div>
                </div>
                 </div>







            <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
<!--      <script src="resource/js/jquery.js"></script>-->


    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>


</body>

</html>
