<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>

<style>
    .form-control,.fff{
        margin-bottom: 20px;
        margin-right: 10px
    }
  
    .form-inline .abc{
    width: 150px;
    }
    .form-inline .xy{
    width: 100px;
    }
    .form-inline .xyz{
    width: 120px;
    }
    .form-control,.dta{
        height: 40px;
    }


</style>
<body>
	<div id="wrapper">
		
		<?php include "view/navbar.php" ?>
		
		<div id="page-wrapper">
			<div class="container-fluid">
	
 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>
   </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Purchase</b></h3>
  </div>
  <div class="card-block">
            <div class="row">
               <div class="col-md-12">
						<div class="form-inline"> 
<!--                <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Purchase ID" v-model="purchase.id">-->
							<input type="text" class="form-control mb-2 mr-sm-2" placeholder="Bill Number" v-model="purchase.bill_number">
							<input type="date" class="form-control mb-2 mr-sm-2 dta" placeholder="Date" v-model="purchase.date">
							<select class="form-control mb-2 mr-sm-2" placeholder="Attribute Type" v-model="purchase.supplier">
								<option disabled value="">Please select a supplier</option>
							    <option v-for="supplier in suppliers" v-bind:value="supplier.id"> {{ supplier.sup_name }} </option>
							  </select>
                
                
                   </div>
                   </div>    
            </div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-inline">
  							<!-- <input type="text" class="form-control mb-2 mr-sm-2 abc" placeholder="Product Name" v-model="newpurchase.name"> -->
  							<select class="form-control mb-2 mr-sm-2" v-model="newpurchase.product_id">
  								<option disabled value="">Select a product</option>
  								<option v-for="product in products" v-bind:value="product.id"> {{ product.product_name }} </option>
  							</select>
  							<select class="form-control mb-2 mr-sm-2" placeholder="Attribute Type" v-model="newpurchase.attribute_type">
  								<option disabled value="">Attribute type</option>
  							    <option value="Color">Color</option>
  							    <option value="size">Size</option>
  							    <option value="make">Make</option>
  							  </select>
  							<input type="text" class="form-control mb-5 mr-sm-5 xy" placeholder="Attribute" v-model="newpurchase.attribute">
  							<input type="number" min="1" class="form-control mb-2 mr-sm-2 xy" placeholder="Quantity" v-model="newpurchase.quantity">
  							<input type="number" min="0" class="form-control mb-2 mr-sm-2 formline xyz" placeholder="Buy Rate" v-model="newpurchase.buy_rate">
  							<input type="number" min="0" class="form-control mb-2 mr-sm-2 xyz" placeholder="Sales Rate" v-model="newpurchase.sales_rate">
  							<input type="number" min="0" class="form-control mb-2 mr-sm-2 xy" placeholder="Total" v-model="newpurchase.total" disabled>
							<button type="button" class="btn btn-light fff" v-on:click="addNewPurchase">Add</button>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						
						<div class="table-responsive">
							<table class="table table-sm table-striped table-hover table-bordered tbl">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Product Name</th>
										<th>Attribute</th>
										<th>Quantity</th>
										<th>Buy Rate</th>
										<th>Sales Rate</th>
										<th>Total</th>
										<th> Action </th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(item, index) in purchase.items">
										<td>{{ index+1 }}</td>
										<td> {{ item.product_name }} </td>
										<td> {{ item.attribute_type + ':  ' + item.attribute }} </td>
										<td> {{ item.quantity }} </td>
										<td> {{ item.buy_rate }} </td>
										<td> {{  item.sales_rate }} </td>
										<td> {{  item.total }} </td>
										<td> <button type="button" class="btn btn-danger" v-on:click="deleteElement(index)"><i class="fa fa-trash"></i></button> </td>
									</tr>
								
								</tbody>
							</table>
						</div>
						<div class="form-inline">
							<input type="text" class="form-control mb-2 mr-sm-2" placeholder="Grand Total" v-model="purchase.grand_total" disabled>
							<input type="number" class="form-control mb-2 mr-sm-2" placeholder="Payment" v-model="purchase.payment">
							<input type="text" class="form-control mb-2 mr-sm-2" placeholder="Due Balance" v-model="purchase.due_balance" disabled>
							<input type="text" class="form-control mb-2 mr-sm-2" placeholder="Description" v-model="purchase.description">
							<select class="form-control mb-2 mr-sm-2" placeholder="Attribute Type" v-model="purchase.pay_mode">
								<option disabled value="" selected>Please select a payment method</option>
							    <option value="cash">Cash</option>nn
							    <option value="cheque">Cheque</option>
							    <option value="card">Card</option>
							  </select>
							<input type="date" class="form-control mb-2 mr-sm-2 dta" placeholder="Due Date" v-model="purchase.due_date">
						</div>
						<div align="center">
	<button type="button" class="btn btn-light" v-on:click="submit" align="center" >Add</button>
                        </div>
					</div>
      </div></div></div></div>
			
			</div>
		<!-- {{ purchase }} -->
		</div>
	<!-- Wrapper -->
	</div>

<footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    <!-- jQuery -->
    <script src="resource/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>
	
	<script type="text/javascript">
		var app = new Vue({
		  el: '#wrapper',
		  data: {
		    // message: 'Hello Vue.js!'
		    newpurchase: {
		    	product_id: '',
		    	product_name: '',
		    	attribute_type: '',
		    	attribute: '',
		    	quantity: '',
		    	buy_rate: '',
		    	sales_rate: '',
		    	total: ''
		    },

		    purchase: {
		    	date: '',
		    	supplier: '',
                bill_number:'',
		    	items: [],
		    	grand_total: '',
		    	payment: 0,
		    	due_balance: '',
		    	description: '',
		    	pay_mode: '',
		    	due_date: ''
		    },

		    suppliers: [],

		    products: [],

		  },

		  methods: {

		  	calculateTotal: function () {
		  		this.newpurchase.total = this.newpurchase.quantity * this.newpurchase.buy_rate;
		  	},

		  	addNewPurchase: function () {
		  		var that1 = this;
		  		if(this.newpurchase.product_id == '' ||
		  			this.newpurchase.product_name == '' ||
		  			this.newpurchase.attribute_type == '' ||
		  			this.newpurchase.attribute == '' ||
		  			this.newpurchase.quantity == '' ||
		  			this.newpurchase.buy_rate == '' ||
		  			this.newpurchase.sales_rate == '' ||
		  			this.newpurchase.total == ''
		  			){
		  			swal("All fields are essential!");
		  			return 0;
		  		} else {
		  			let npObj = {};
		  			npObj.product_id = this.newpurchase.product_id;
		  			npObj.product_name = this.newpurchase.product_name;
		  			npObj.attribute_type = this.newpurchase.attribute_type;
		  			npObj.attribute = this.newpurchase.attribute;
		  			npObj.quantity = this.newpurchase.quantity;
		  			npObj.buy_rate = this.newpurchase.buy_rate;
		  			npObj.sales_rate = this.newpurchase.sales_rate;
		  			npObj.total = this.newpurchase.total;
			  		this.purchase.items.push(npObj);
			  		
			  		setTimeout(function(){
				  		that1.newpurchase.name = '',
				  		that1.newpurchase.attribute_type = '',
				  		that1.newpurchase.attribute = '',
				  		that1.newpurchase.quantity = '',
				  		that1.newpurchase.buy_rate = '',
				  		that1.newpurchase.sales_rate = '',
				  		that1.newpurchase.total = ''
			  		 }, 500);
		  		}
		  		// clear data after data addition
		  	},

		  	deleteElement(index) {
		  		this.purchase.items.splice(index, 1);
		  	},

		  	// submit event
		  	submit() {
		  		var that2 = this;
		  		if (that2.purchase.date === ''
		  			|| that2.purchase.supplier === ''
		  			|| that2.purchase.items.length === 0
		  			|| that2.purchase.grand_total === ''
		  			|| that2.purchase.due_balance === ''
		  			|| that2.purchase.description === ''
		  			|| that2.purchase.pay_mode === ''
		  			|| that2.purchase.due_date === '') {
		  			swal("All fields are require.");
		  			return 0;
		  		} else {
		  			$.ajax({
		  			        type: "POST",
		  			        data: {data:that2.purchase},
							url: "restcontroller/newpost.php",
		  			        success: function(data){
		  			           		swal(data);
		  			           		that2.purchase.date= '',
		    						that2.purchase.supplier= '',
		    						that2.purchase.items= [],
							    	that2.purchase.grand_total= '',
							    	that2.purchase.payment= 0,
							    	that2.purchase.due_balance= '',
							    	that2.purchase.description= '',
							    	that2.purchase.pay_mode= '',
							    	that2.purchase.due_date= '',
                                    that2.purchase.bill_number= ''
		  			               },
		  			           error: function(error) {
		  			           		swal(error);
		  			           }
		  			    });
		  			return 0;
		  		}
		  	},
		  	fetchSuppliers: function () {
		  		var that3 = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fetchSuppliers.php",
		  			success: function(data) {
		  				that3.suppliers = JSON.parse(data);
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	},

		  	fetchProducts: function () {
		  		var that4 = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fetchProducts.php",
		  			success: function (data) {
		  				that4.products = JSON.parse(data);
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	}
		  },

		  mounted: function () {
		  	/*this.sayHello();*/
		  	this.fetchSuppliers();

		  	this.fetchProducts();
		  },

		  watch: {
		  	'newpurchase.quantity': function () {
		  		this.calculateTotal();
		  	},
		  	'newpurchase.buy_rate': function () {
		  		this.calculateTotal();
		  	},
		  	'purchase.items': {
		  		handler: function () {
		  			var gt = 0;
		  			$.each(this.purchase.items, function(index, value) {
		  				gt = gt + value.total;
		  			});
		  			this.purchase.grand_total = gt;
		  		},
		  		deep: true
		  	},
		  	'purchase.grand_total': function () {
		  		this.purchase.due_balance = this.purchase.grand_total - this.purchase.payment;
		  	},
		  	'purchase.payment': function () {
		  		if(this.purchase.payment > this.purchase.grand_total){
		  			this.purchase.payment = this.purchase.grand_total;
		  		}
		  		if(this.purchase.payment < 0) {
		  			this.purchase.payment = 0;
		  		}
		  		this.purchase.due_balance = this.purchase.grand_total - this.purchase.payment;
		  	},
		  	'newpurchase.product_id': function () {
		  		var that = this;
		  		$.each(this.products, function(index, value) {
		  			if(that.newpurchase.product_id == value.id){
		  				that.newpurchase.product_name = value.product_name;
		  			}
		  		});
		  	}
		  }
		})
	</script>
</body>

</html>