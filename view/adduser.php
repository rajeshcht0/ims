<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>
<?php 
    if(isset($_POST["adduser"]))
    {
          $photo=addslashes(file_get_contents($_FILES["photo"]["tmp_name"]));
          //  $photo=file_get_contents($photo);
          //  $photo=base64_encode($photo);
          $nationalitycard=addslashes(file_get_contents($_FILES["nationalitycard"]["tmp_name"]));
          //  $nationalitycard=file_get_contents($photo);
         //   $nationalitycard=base64_encode($photo);
            $uid=$_POST["userid"];
            $name=$_POST["name"];
            $address=$_POST["address"];
            $email=$_POST["email"];
            $nationalaity=$_POST["nationality"];
            $contact1=$_POST["contact1"];
            $contact2=$_POST["contact2"];
            $access=$_POST["access"];
            $password =$_POST["password"];
        
        $hashFormat = "$2y$10$";
        $salt = "njhytrfedsarewrtyuhg77";
        $hashF_and_salt = $hashFormat . $salt;
        $password= crypt($password,$hashF_and_salt);
           $query = "INSERT INTO user(user_id,user_name,user_address,user_email,user_password,user_nationality,user_contact1,user_contact2,access,user_photo,nationality_card) VALUES('$uid', '$name', '$address', '$email', '$password', '$nationalaity', '$contact1', '$contact2', '$access', '$photo', '$nationalitycard')"; 
            if(!mysqli_query($connection,$query)) {
                die('Query FAILED'. mysqli_error());
            }
        else{
            header("location:index.php?r=viewuser");
        }
     
    }
?>

<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
               <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>
 </div>
                    <div class="card text-center">
  <div class="card-header card-primary card-inverse" width="100%">
      <h3 align="center" style="color:white"><b>Add User</b></h3>
  </div>
  <div class="card-block" bgcolor="red">
<!-- page body-->
    <div class="row">
    <div class="col-lg-6 col-md-offset-3">
 <form action="" method="post" enctype="multipart/form-data">
       <div class="form-group">
        <lable for="name">Full Name</lable> 
        <input type="text" name="name" class="form-control" required></div>
        
        <div class="form-group">
        <lable for="address">Address</lable> 
        <input type="text" name="address" class="form-control" required></div>
        
      <div class="form-group">
        <lable for="email">Email</lable> 
          <input type="email" name="email" class="form-control" required></div>
          
          <div class="form-group">
        <lable for="userid">User ID</lable> 
        <input type="text" name="userid" class="form-control" required></div>
        
    <div class="form-group">
        <lable for="password">Password</lable> 
        <input type="password" name="password" class="form-control" required></div>
        
        <div class="form-group">
        <lable for="nationality">Nationality</lable> 
        <input type="text" name="nationality" class="form-control" required></div>
        
        <div class="form-group">
        <lable for="nationalitycard">Nationality card</lable> 
        <input type="file" name="nationalitycard" class="form-control" required></div>
        
        <div class="form-group">
        <lable for="contact1">Contact 1</lable> 
        <input type="text" name="contact1" class="form-control" required></div>
        <div class="form-group">
        <lable for="contact2">contact 2</lable> 
        <input type="text" name="contact2" class="form-control"></div>
        
        <div class="form-group">
        <lable for="photo">User Image</lable> 
        <input type="file" name="photo" class="form-control" required></div>
        
        <div class="form-group">
  <label for="sel1">Access:</label>
  <select class="form-control" id="sel1" name="access">
    <option>Admin</option>
    <option>Manager</option>
    <option>User</option>
  </select>
</div>
        <input  class="btn btn-primary" type="submit" value="ADD User" name="adduser">
        <input class="btn btn-primary" type="reset" value="reset">
 </form>
 </div>
  </div>
 
</div>
                </div>
                 </div>
                 

        </div>
<!--footer start-->
            <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>

    <!-- jQuery -->
    <script src="resource/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>

</body>

</html>
