<nav class="navbar navbar-dark bg-inverse navbar-fixed-top">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button class="navbar-toggler hidden-sm-up pull-sm-right" type="button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                 </button>
                <a class="navbar-brand" href="#"></a>
                <img align="left" src="resource/img/s.gif" width="10%" height="50px">

                <img align="left" src="resource/img/inventory.png" width="70%" height="50px"alt="">


            </div>
                <!-- Top Menu Items -->

            <ul class="nav navbar-nav top-nav navbar-right pull-xs-right">

         <li class="dropdown nav-item">
                    <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['user_name']; ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-item">
                            <a href="index.php?r=userprofile&userid=<?php echo $_SESSION['user_id']; ?>"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li class="dropdown-item">
                            <a href="index.php?r=logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-toggleable-sm navbar-ex1-collapse" allign="right">
                <ul class="nav navbar-nav side-nav list-group">
                    <li class="list-group-item">
                        <a href="<?= $base_url ?>?r=dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo5"><i class="fa fa-fw fa-list-alt"></i> Product <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo5" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=addproduct"><i class="fa fa-fw fa-plus-circle"></i> Add Product</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewproduct"><i class="fa fa-fw fa-eye"></i> View Product</a>
                            </li>
                       </ul>
                    </li>

                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo0"><i class="fa fa-fw fa-group"></i> Customer <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo0" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=addcustomer"><i class="fa fa-fw fa-plus-circle"></i> Add Customer</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewcustomer"><i class="fa fa-fw fa-eye"></i> View Customer</a>
                            </li>
                       </ul>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo2"><i class="fa fa-fw fa-truck"></i> Suppliers <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo2" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=addsuppliers"><i class="fa fa-fw fa-plus-circle"></i> Add Suppliers</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewsuppliers"><i class="fa fa-fw fa-eye"></i> View Suppliers</a>
                            </li>
                       </ul>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo3"><i class="fa fa-fw fa-align-justify"></i> Stock <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo3" class="list-group collapse">

                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=purchase"><i class="fa fa-fw fa-plus-circle"></i> Purchase Stock</a>
                            </li><li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewpurchase"><i class="fa fa-fw fa-eye"></i> View Purchase</a>
                            </li><li class="list-group-item">
                                <a href="<?= $base_url ?>?r=stock"><i class="fa fa-fw fa-eye"></i> View Stock Availible</a>
                            </li>
                       </ul>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo4"><i class="fa fa-fw fa-shopping-cart"></i> Sales <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo4" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=sales"><i class="fa fa-fw fa-shopping-cart"></i> Sales </a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewsales"><i class="fa fa-fw fa-eye"></i> View Sales</a>
                            </li>
                       </ul>
                    </li>
                    <li class="list-group-item">
                        <a href="<?= $base_url ?>?r=payment"><i class="fa fa-fw fa-rupee"></i> Payment</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?= $base_url ?>?r=outstanding"><i class="fa fa-fw fa-table"></i> Outstanding</a>
                    </li>
                    <li class="list-group-item">
                        <a href="<?= $base_url ?>?r=pending"><i class="fa fa-pause"></i> Pending</a>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo1"><i class="fa fa-fw fa-file-excel-o"></i> Report <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo1" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=report"><i class="fa fa-line-chart"></i> Sales And Product</a>
                            </li>
                       </ul>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-gear"></i> Setting <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="list-group collapse">
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=viewuser"><i class="fa fa-fw fa-list-alt"></i> User Record</a>
                            </li>
                            <li class="list-group-item">
                                <a href="<?= $base_url ?>?r=adduser"><i class="fa fa-fw fa-user"></i> Add User</a>
                            </li>
                       </ul>
                    </li>

                </ul>
            </div>
                   <script src="resource/js/jquery.js"></script>

            <!-- /.navbar-collapse -->
        </nav>
