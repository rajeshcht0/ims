<?php 
if (isset($_SESSION['login'])) {
    redirect('dashboard');
    return;
}
?>
  <html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IMS</title>

    <!-- Bootstrap core CSS -->
    <link href="resource/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link rel="icon" href="resource/img/favicon.png" type="image/png">

    

    <!-- Custom styles for this template -->
    <link href="resource/css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php?r=login">Start IMS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="index.php?r=about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="index.php?r=services">Services</a>
            </li>
           
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="index.php?r=contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead">
      <div class="header-content">
        <div class="header-content-inner">
            <h1 id="homeHeading">wel-come to</h1> <br/> <h1>inventory management system</h1>
          <hr>
          <p>With our powerful inventory control and order management features, you can increase sales and fulfill orders efficiently.</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="index.php?r=login">GET STARTED</a>
        </div>
      </div>
    </header>
    
    <!-- Bootstrap core JavaScript -->
    <script src="resource/vendor/jquery/jquery.min.js"></script>
   <script src="resource/vendor/bootstrap/js/bootstrap.min.js"></script>


    <!-- Custom scripts for this template -->
    <script src="resource/js/creative.min.js"></script>

  </body>

</html>
