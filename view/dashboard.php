<?php include "view/headers.php" ?>
<?php include "model/db.php "?>
<?php include "restcontroller/claculate.php" ?>
<?php
 $query = "SELECT product.product_name,product_purchase.attribute_type,product_purchase.attribute,sum(product_purchase.quantity) FROM product_purchase LEFT JOIN product ON product.id=product_purchase.product_id GROUP BY product.product_name
";
 $result = mysqli_query($connection, $query);
$query2 ="SELECT product.product_name,product_purchase.attribute_type,product_purchase.attribute,product_purchase.buy_rate,product_purchase.sales_rate FROM product_purchase LEFT JOIN product ON product_purchase.product_id=product.id";
 $result2 = mysqli_query($connection, $query2);
$rows = array();
$table = array();
$table['cols'] = array(
 array(
  'label' => 'Product',
  'type' => 'string'
 ),
 array(
  'label' => 'buyrate',
  'type' => 'number'
 ),
     array(
  'label' => 'sallrate',
  'type' => 'number'
 )
);

while($row = mysqli_fetch_array($result2))
{
 $sub_array = array();
 $datetime = explode(".", $row["product_name"]);
 $sub_array[] =  array(
      "v" => '(' . $datetime[0] . ')'
     );
 $sub_array[] =  array(
      "v" => $row["buy_rate"]
     );
    $sub_array[] =  array(
      "v" => $row["sales_rate"]
     );
 $rows[] =  array(
     "c" => $sub_array,

    );
}
$table['rows'] = $rows;
$jsonTable = json_encode($table);
 ?>
<?php include 'Helper/SessionHelper.php';?>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
           <script type="text/javascript">
           google.charts.load('current', {'packages':['bar']});
           google.charts.setOnLoadCallback(drawStuff);
           function drawStuff()
           {
                var data = google.visualization.arrayToDataTable([
                          ['product_name', 'quantity'],
                          <?php
                          while($row = mysqli_fetch_array($result))
                          {
                               echo "['".$row["product_name"]."', ".$row["sum(product_purchase.quantity)"]."],";
                          }
                          ?>
                     ]);
                var options = {
          title: 'Chess opening moves',
          width: 600,
          height: 300,
          legend: { position: 'none' },
         // chart: { title: 'Product Stocks',
         //          subtitle: 'quantity of product in the stock' },
          bars: 'vervical', // Required for Material Bar Charts.
          axes: {
            x: {
            //  0: { side: 'top'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };
                var chart = new google.charts.Bar(document.getElementById('piechart'));
                chart.draw(data, options);
           }

           </script>
           <script type="text/javascript">
   google.charts.load('current', {'packages':['corechart']});
   google.charts.setOnLoadCallback(drawChart);
   function drawChart()
   {
    var data = new google.visualization.DataTable(<?php echo $jsonTable; ?>);

    var options = {
     //title:'Sensors Data',
     legend:{position:'bottom'},
     chartArea:{width:'95%', height:'65%'}
    };

    var chart = new google.visualization.LineChart(document.getElementById('line_chart'));

    chart.draw(data, options);
   }
  </script>
     <style>

    .dta{
        height: 40px;
    }


</style>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>



        <div id="page-wrapper">
        <div>

              <?php
            if (hasFlash('message')) {
                $falshError = getFlash('message');
                foreach ($falshError as $fe) {
                    ?>
                    <div class="alert alert-<?php echo $fe['type']; ?> fade in alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <?php
                        echo empty($fe['title']) ? '' : "<strong>" . $fe['title'] . "</strong> ";
                        echo $fe['body'];}}
                        ?>
        </div>

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>


                    </div>
      <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Company Performance Page</b></h3>
  </div>
  <div class="card-block">
  <div class="row">
  <div class="col-sm-8">
<div class="card" style="width: 100%;">
 <div class="card-header card-primary dta">
      <h4 align="center" style="color:white"> STOCK DETAILS CHART</h4>
  </div>
  <div class="card-block">

   <div style="width:900px;">
       <div id="piechart" style="width: 900px; height: 300px;"></div>
           </div>
  </div>
      </div></div>
  <div class="col-sm-4">
<div class="card" style="width: 100%;">
 <div class="card-header card-primary dta">
      <h4 align="center" style="color:white"> LOW STOCK ITEMS
</h4>
  </div>
  <div class="card-block" style="height: 340px">
<!--       <div style="width: 900px; height: 300px;"></div> -->
          <table class="table table-bordered">
              <thade>
                  <tr>
                      <th>Product</th>
                      <th>Stock Qty</th>
                  </tr>
              </thade>
              <tbody>
                 <?php
                  $sel="SELECT product.product_name,product_purchase.attribute_type,product_purchase.attribute,product_purchase.quantity FROM product_purchase LEFT JOIN product ON product.id=product_purchase.product_id ORDER BY quantity ASC LIMIT 5";
                  $res=mysqli_query($connection,$sel);
                       while($val=mysqli_fetch_array($res,MYSQLI_ASSOC)){

                  ?>
                  <tr>
        <td><?php echo $val['product_name']; ?><small> <?php echo $val['attribute_type']; ?> <?php echo $val['attribute']; ?></small></td>
         <td><?php echo $val['quantity']; ?></td>
                  </tr>
                  <?php } ?>
              </tbody>
          </table>
           </div>
  </div>
</div>
      </div>
</div>
          <div class="row">
  <div class="col-sm-8">
<div class="card" style="width: 100%;">
 <div class="card-header card-primary dta">
      <h4 align="center" style="color:white">COMPANY PRODUCT PRICE CHART</h4>
  </div>
  <div class="card-block">

   <div style="width:800px;">
       <div id="line_chart" style="width: 80%; height: 300px;"></div>
           </div>
  </div>
      </div></div>
  <div class="col-sm-4">
<div class="card" style="width: 100%;">
 <div class="card-header card-primary dta">
      <h4 align="center" style="color:white">COMPANY STATUS</h4>
  </div>
  <div class="card-block">

   <table class="table table-bordered">
       <thead>
           <tr>
               <th>Operation Name</th>
               <th>Operation</th>
           </tr>
       </thead>
       <tbody>
           <tr>
               <td>Total Number of Stocks</td>
               <td><?php stockcount(); ?></td>
           </tr>
           <tr>
        <td>Tatal Sales Transactions</td>
           <td><?php salestransactions(); ?></td>

           </tr>
           <tr>
           <td>Total number of Suppliers</td>
            <td> <?php suppliercount(); ?></td>
           </tr>
           <tr>
           <td>Total Number of Customers</td>
            <td> <?php customercount(); ?></td>
           </tr>
       </tbody>
   </table>
  </div>
</div>
      </div>
</div>
          </div>
                </div>
                 </div>
    <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="resource/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>

</body>

</html>
