<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>
 <div id="page-wrapper">
 <div class="container-fluid">
 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>
   </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Add Product Details</b></h3>
  </div>
  <div class="card-block">
  <form action="" method="post" >

    <div class="row">
 <div class="col-lg-4 form-inline">
       <div class="form-group">
        <lable for="category">Category</lable> 
        <input type="text" name="category" class="form-control" required></div>
         </div>
    <div class="col-lg-4 form-inline">
       <div class="form-group">
        <lable for="model">Model</lable> 
        <input type="text" name="model" class="form-control"></div> 
         </div>
         <div class="col-lg-4 form-inline">
       <div class="form-group">
        <lable for="modelno">Model No.</lable> 
        <input type="text" name="modelno" class="form-control"></div>
         </div>
    </div>
      <br>
       <div class="row">
 <div class="col-lg-6 form-inline" align="center">
       <div class="form-group" alien="center">
        <lable for="brand">Brand</lable> 
        <input type="text" name="brand" class="form-control" required></div>
         </div>
    <div class="col-lg-6 form-inline">
       <div class="form-group">
        <lable for="manufacturer">Manufacturer</lable> 
           <textarea name="manufacturer" class="form-control" required></textarea>
         </div>
        </div></div><br>
        <div class="row">
 <div class="col-lg-6 form-inline" align="center">
       <div class="form-group">
        <lable for="productname">Product Name</lable> 
        <input type="text" name="productname" class="form-control" required></div>
         </div>
    <div class="col-lg-6 form-inline">
       <div class="form-group">
        <lable for="productdiscription">Product Discription</lable> 
           <textarea name="productdiscription" class="form-control" required></textarea>
         </div>
        </div></div><br>
      <div class="row">
 <div class="col-lg-6 form-inline" align="center">
       <div class="form-group" alien="center">
        <lable for="warrentyperiod">Warrenty Period</lable> 
        <input type="text" name="warrentyperiod" class="form-control" required></div>
         </div>
    <div class="col-lg-6 form-inline">
       <div class="form-group">
        <lable for="warrentyterms">Warrenty Terms</lable> 
           <textarea name="warrentyterms" class="form-control" required></textarea>
         </div>
        </div></div><br>
        <div align="center">
             <input  class="btn btn-primary" type="submit" value="Add Product Details" name="addcustomer">
        <input class="btn btn-primary" type="reset" value="reset">
            
        </div>
    
     </form>

</div>
</div>
   </div>
    <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="resource/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>

</body>

</html>
