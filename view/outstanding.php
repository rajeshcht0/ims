<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>


<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>


                    </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Sells Outstanding</b></h3>
  </div>
  <div class="card-block">




    <div class="container" style="margin-top:35px">

        <table id="mytable" class="table table-bordered table-striped">

     <thead>
            <tr>

                <th>Bill No</th>
                <th>customer</th>
                <th>Sells Date</th>
                <th>Due Date</th>
                <th>Due Amount</th>
                 <th>Total</th>
                 <th>Pay Mode</th>

                 <th>Action</th>

            </tr>
        </thead>

        <tbody>
            <?php ;
$sel ="SELECT customer.cus_name,sales.due_date,sales.id,sales.billno,sales.date,sales.grand_total,sales.due_balance,sales.paymode FROM sales LEFT JOIN customer ON customer.id=sales.costumer WHERE sales.due_balance > 0
";
$res =mysqli_query($connection,$sel);
     while($val=mysqli_fetch_array($res,MYSQLI_ASSOC)){
         ?>
       <tr>

         <td><?php echo $val['billno']; ?></td>
         <td><?php echo $val['cus_name']; ?></td>
         <td><?php echo $val['date']; ?></td>
         <td><?php echo $val['due_date']; ?></td>
         <td><?php echo $val['due_balance']; ?></td>
        <td><?php echo $val['grand_total']; ?></td>
         <td><?php echo $val['paymode']; ?></td>

         <td align="center"> <a href=index.php?r=payoutstanding&id=<?php echo $val['id']; ?>><i class="fa fa-fw fa-money" ></i></a></td>

         <?php  ;} ?>






 <!-- //  echo "<tr><td>1</td><td>$val[user_id]</td><td>$val[user_name]</td><td>$val[user_address]</td><td>$val[user_contact1]</td><td><a href='delete.php?user_id=$val[user_id]'>Delete</td>
<td><a href='update.php?user_id=$val[user_id]'>update</td></tr>";-->

 </tbody>
        </table>
              <script>
    $(document).ready(function() {
    $('#mytable').DataTable();
} ); </script>
<script src="resource\DataTables_3\dataTables.min.js"></script>
</div>
                </div>
                 </div>







            <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
<!--      -->


    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>


</body>

</html>
