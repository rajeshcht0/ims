<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>IMS-inventory management system</title>
    <link rel="icon" href="resource/img/favicon.png" type="image/png">

    <!-- Bootstrap Core CSS -->
 <link rel="stylesheet" type="text/css" href="resource/css/bootstrap.css">

    <!-- Custom CSS -->
    <link href="resource/css/sb-admin.css" rel="stylesheet">
        <link href="resource\DataTables_3\datatables.min.css" rel="stylesheet">

   
   
   
    <link href="resource/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="<?php echo 'resource/js/vue.js'; ?>"></script>
    <script src="<?php echo 'resource/js/sweetalert.min.js'; ?>"></script>
      <script type="text/javascript" src="resource/js/jquery-3.2.1.js"></script>
       </head>
