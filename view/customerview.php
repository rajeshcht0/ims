<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>


<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>


                    </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Customer List</b></h3>
  </div>
  <div class="card-block">




    <div class="container" style="margin-top:35px">
        
        <table id="mytable" class="table table-bordered table-striped">

     <thead>
            <tr>

                <th>Customer ID</th>
                <th>name</th>
                <th>address</th>
                <th>Contact</th>
                <th>E-mail</th>
                 <th>action</th>
            </tr>
        </thead>

        <tbody>
            <?php 
$sel ="select id,cus_id,cus_name,cus_address,contact1,cus_email from customer";
$res =mysqli_query($connection,$sel);
     while($val=mysqli_fetch_array($res,MYSQLI_ASSOC)){
         ?>
       <tr>

         <td><?php echo $val['cus_id']; ?></td>
         <td><?php echo $val['cus_name']; ?></td>
         <td><?php echo $val['cus_address']; ?></td>
         <td><?php echo $val['contact1']; ?></td>
          <td><?php echo $val['cus_email']; ?></td>
        <td > <a href="index.php?r=updatecustomer&id=<?php echo $val['id']; ?>"><i class="fa fa-fw fa-pencil"></i></a> <a href=view/delete.php?id=<?php echo $val['id']; ?>&table=customer&return=index.php?r=viewcustomer><i class="fa fa-fw fa-trash"></i></a></td>

         <?php  ;} ?>
 </tbody>
        </table>
        <div class="pagination-container">
            <nav>
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>

           <script>
    $(document).ready(function() {
    $('#mytable').DataTable();
} ); </script>
<script src="resource\DataTables_3\dataTables.min.js"></script>
</div>
                </div>
                 </div>







            <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
<!--      <script src="resource/js/jquery.js"></script>-->


    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>


</body>

</html>
