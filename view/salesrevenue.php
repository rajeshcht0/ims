<?php

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sales Print</title>
    <link rel="icon" href="Knight/favicon.png" type="image/png">
    <style type="text/css" media="print">
.hide{display:none}
</style>
<script type="text/javascript">
function printpage() {
document.getElementById('printButton').style.visibility="hidden";
window.print();
document.getElementById('printButton').style.visibility="visible";
}
</script>
<style type="text/css">
.style1 {font-size: 10px}
</style>
</head>
<body>
<input name="print" type="button" value="Print" id="printButton" onClick="printpage()">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top">
	<table width="595"  cellspacing="0" cellpadding="0" id="bordertable"  border="1">
      <tr>
        <td align="center"><strong>Sales Receipt <br />
        </strong>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="67%" align="left" valign="top">&nbsp;&nbsp;&nbsp;Date: 25/02/2018 <br />
                <br />
                <strong><br />
                &nbsp;&nbsp;&nbsp;Receipt No: SA73                </strong><br />
              </td>
              <td width="33%"><div align="center"><span class="style1"></span> <br />
                  <strong>UK Electronics LTD </strong><br />
                  Mirpur-2, <br />
                  Dhaka-1216<br />
                 <br />
              </div></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td height="90" align="left" valign="top"><br />
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="5%" align="left" valign="top"><strong>&nbsp;&nbsp;TO:</strong></td>
              <td width="95%" align="left" valign="top"><br />
              Rakib				<br />
				Rangpur<br>Contact1: 365465165453<br>Contact1: 64564525695<br></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="12%" align="center" bgcolor="#CCCCCC"><strong>No.</strong></td>
            <td width="22%" bgcolor="#CCCCCC"><strong>Stock</strong></td>
            <td width="18%" bgcolor="#CCCCCC"><strong>Quantity</strong></td>
            <td width="19%" bgcolor="#CCCCCC"><strong>Rate</strong></td>
            <td width="11%" bgcolor="#CCCCCC">&nbsp;</td>
            <td width="18%" bgcolor="#CCCCCC"><strong>Total</strong></td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		            <tr>
            <td align="center">1.</td>
            <td>Dell-Monitor505</td>
            <td>100.00</td>
            <td>7500.00</td>
            <td>&nbsp;</td>
            <td>750000.00</td>
          </tr>

		            <tr>
            <td align="center">2.</td>
            <td>dell-laptop</td>
            <td>50.00</td>
            <td>55000.00</td>
            <td>&nbsp;</td>
            <td>2750000.00</td>
          </tr>

		            <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
        </table></td>
      </tr>
	  <tr>
	  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="82%" align="right" bgcolor="#CCCCCC"><strong>SubTotal:&nbsp;&nbsp;</strong></td>
          <td width="18%" bgcolor="#CCCCCC">3500000.00&nbsp;</td>
        </tr>
      </table>	  </td>
	  </tr>
      <tr>
        <td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="33%" align="left" valign="top"><br />
              <strong>&nbsp;&nbsp;Paid Amount :&nbsp;&nbsp;70000.00<br />
              &nbsp;&nbsp;Due Balance &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;:&nbsp;&nbsp;680000.00<br />
              &nbsp;&nbsp;Due Date&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;: 25/02/2018 <br />
              </strong> </td>
            <td width="67%" align="right"><br />
              <br />
              <br />
              Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>

    </table></td>
  </tr>
</table>


</body>
</html>
