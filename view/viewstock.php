<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>


<body>

    <div id="wrapper">
        <!-- Navigation -->
        <?php include "view/navbar.php" ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                      <h1 align="center" class="page-header">
                        <small> </small>
                    </h1>


                    </div>
                    <div class="card text-center ">
  <div class="card-header card-primary card-inverse">
      <h3 align="center" style="color:white"><b>Purchase Stock List</b></h3>
  </div>
  <div class="card-block">





        <table id="mytable" class="table table-bordered table-striped">

     <thead>
            <tr>

                <th>product</th>

                <th>attribute type</th>
                 <th>attribute</th>
                 <th>Quantity</th>
                <!-- <th>Action</th> -->


            </tr>
        </thead>

        <tbody>
            <?php ;
$sel ="SELECT product.product_name,product_purchase.attribute_type,product_purchase.attribute,product_purchase.quantity FROM product_purchase LEFT JOIN product ON product.id=product_purchase.product_id
 ORDER BY product_purchase.quantity DESC ";
$res =mysqli_query($connection,$sel);
     while($val=mysqli_fetch_array($res,MYSQLI_ASSOC)){
         ?>
       <tr>

         <td><?php echo $val['product_name']; ?></td>

         <td><?php echo $val['attribute_type']; ?></td>
        <td><?php echo $val['attribute']; ?></td>
         <td><?php echo $val['quantity']; ?></td>


        <!-- <td > <a href=edituser.php?id=<?php echo $val['id']; ?>><i class="fa fa-fw fa-pencil"></i></a> <a href=delete.php?id=<?php echo $val['id']; ?>&table=user&return=viewuser.php><i class="fa fa-fw fa-trash"></i></a></td>      -->

         <?php  ;} ?>






 <!-- //  echo "<tr><td>1</td><td>$val[user_id]</td><td>$val[user_name]</td><td>$val[user_address]</td><td>$val[user_contact1]</td><td><a href='delete.php?user_id=$val[user_id]'>Delete</td>
<td><a href='update.php?user_id=$val[user_id]'>update</td></tr>";-->

 </tbody>
        </table>
        <div class="pagination-container">
            <nav>
                <ul class="pagination"></ul>
            </nav>
        </div>
    </div>

           <script>
    $(document).ready(function() {
    $('#mytable').DataTable();
} ); </script>
<script src="resource\DataTables_3\dataTables.min.js"></script>
</div>
                </div>
                 </div>







            <!-- /.container-fluid -->
            </div>
        </div>
        <!-- /#page-wrapper -->
 <footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    <!-- /#wrapper -->

    <!-- jQuery -->
<!--      <script src="resource/js/jquery.js"></script>-->


    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="resource/js/plugins/morris/raphael.min.js"></script>
    <script src="resource/js/plugins/morris/morris.min.js"></script>
    <script src="resource/js/plugins/morris/morris-data.js"></script>


</body>

</html>
