<?php include "view/headers.php" ?>
<?php include 'Helper/SessionHelper.php';?>
<?php include "model/db.php" ?>
<!--
<style>
    .form-control.abc{
        width: 100px;
    }
</style>
-->
<body>
	<div id="wrapper">
		<?php include "view/navbar.php" ?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
						  <div class="card-header bg-primary" style="text-align: center;"><h1>Sales</h1></div>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-group col-md-4">
						<input class="form-control" type="number" min="1" placeholder="Bill Number" v-model="bill.number">
					</div>
					<div class="form-group col-md-4">
						<!-- <input class="form-control" type="text" placeholder="Customer Name" v-model="bill.customer"> -->
						<select class="form-control mb-2 mr-sm-2" placeholder="Customer" v-model="bill.customer_id">
							<option disabled value="">Select a customer</option>
						    <option v-for="customer in customers" v-bind:value="customer.id">{{customer.cus_name}}</option>
						  </select>
					</div>
					<div class="form-group col-md-4">
						<input class="form-control" type="date" placeholder="Date" v-model="bill.date">
					</div>
				</div>
				<hr>
				<div class="form-row">
					<div class="form-group col-md-2">
						<select class="form-control mb-2 mr-sm-2" placeholder="Product" v-model="filter.product">
							<option disabled value="">Select a product</option>
						    <option v-for="product in products" v-bind:value="product.id">{{product.product_name}}</option>
						  </select>
					</div>
					<div class="form-group col-md-2">
						<select class="form-control mb-2 mr-sm-2" placeholder="Attribute Type" v-model="filter.attribute_type">
							<option disabled value="">Select an attribute type</option>
						    <option v-for="attribute_type in attribute_types" v-bind:value="attribute_type.attribute_type">{{attribute_type.attribute_type}}</option>
						  </select>
					</div>
					<div class="form-group col-md-2">
						<select class="form-control mb-2 mr-sm-2" placeholder="Attribute" v-model="filter.attribute">
							<option disabled value="">Select an attribute</option>
						    <option v-for="attribute in attributes" v-bind:value="attribute.attribute">{{attribute.attribute}}</option>
						  </select>						
					</div>
					<div class="form-group col-md-3">
						<!-- <p>14 Pieces @ Rs. 12 </p> -->
						<select class="form-control mb-2 mr-sm-2" placeholder="Purchase" v-model="filter.purchase">
							<option disabled value="">Select a purchase</option>
						    <option v-for="purchase in purchases" v-bind:value="purchase.purchase_id">{{ purchase.quantity }} Pieces of {{purchase.product_name}} @ Rs. {{ purchase.sales_rate }} each.</option>
						  </select>
					</div>
					<div class="form-group col-md-2">
						<input class="form-control mb-2 mr-sm-2" type="number" min="1" placeholder="Quantity" v-model="filter.quantity">
					</div>
					<div class="form-group col-md-1">
						<button class="btn btn-primary" v-on:click="addPurchase(filter.purchase)">
							Add
						</button>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table">
							<thead>
								<tr>
									<th>S.No.</th>
									<th>Name</th>
									<th>Attribute</th>
									<th>Quantity</th>
									<th>Sales Rate</th>
									<th>Total</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(item, index) in purchase_list">
									<td>{{index+1}}</td>
									<td> {{ item.product_name }} </td>
									<td>{{ item.attribute_type }}: {{ item.attribute }}</td>
									<td> {{ item.quantity }} </td>
									<td> {{ item.sales_rate | twodigitdecimal}} </td>
									<td> {{ item.quantity * item.sales_rate | twodigitdecimal}} </td>
									<td> <button type="button" class="btn btn-danger" v-on:click="deleteElement(index)"><i class="fa fa-trash"></i></button> </td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-2">
						Payment: <input class="form-control abc" type="number" min="0" placeholder="Payment" v-model="bill.payment"> 
                        Due Amount:<b> {{bill.due_amount | twodigitdecimal}} </b>
					</div>
					<div class="col-md-3">
						Payment Method:
						<select class="form-control mb-2 mr-sm-2" v-model="bill.payment_method">
							<option disabled value="">Payment Method</option>
						    <option value="cash">Cash</option>
						    <option value="cheque">Cheque</option>
						    <option value="card">Card</option>
						  </select>
					</div>

					<div class="col-md-3">
						Due Date: <input class="form-control" type="date" placeholder="Due Date" v-model="bill.due_date">
					</div>
					<div class="col-md-2  col-md-offset-2">
						<div class="panel panel-default">
							<p>
                                Total:<b> {{ bill.total | twodigitdecimal}}</b>
							</p>
							<p>
                                VAT(+14%):<b> {{ bill.total * .14 | twodigitdecimal}}</b>
							</p>
							<p>
                                Grand Total:<b> {{ bill.grandtotal | twodigitdecimal}}</b>
							</p>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-1 col-md-offset-5">
						<button class="btn btn-primary" v-on:click="submit">
							Submit
						</button>
					</div>
				</div>
		</div>
 	</div>
	<!-- Wrapper -->
</div>
<footer class="footer" >
            <div class="container">
                <div class="footer-logo"><a href="#"><img src="" alt=""></a></div>
                <span class="copyright">Copyright © 2018 | <a href="http://www.rajeshadhikari.com.np">RRS Developers</a> </span>
            </div>
        </footer>
    <!-- jQuery -->
    <script src="resource/js/jquery.js"></script>

    <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="resource/js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		var app = new Vue({
		  el: '#wrapper',
		  data: {
		  	products: [],
		  	attribute_types: [],
		  	attributes: [],
		  	purchases: [],
		  	purchase_list: [],
		  	customers: [],
		  	filter: {
		  		product: '',
		  		attribute_type: '',
		  		attribute: '',
		  		purchase: '',
		  		quantity: ''
		  	},
		  	bill: {
		  		number: '',
		  		customer_id: '',
		  		date: '',
		  		total: 0,
		  		grandtotal: 0,
		  		payment: 0,
		  		due_amount: 0,
		  		payment_method: '',
		  		due_date: ''
		  	}
		  },

		  methods: {
		  	submit() {
		  		var that = this;

		  		if(
		  			this.bill.number == '' ||
		  			this.bill.customer_id == '' ||
		  			this.bill.date == '' ||
		  			this.bill.total == '' ||
		  			this.bill.grandtotal == '' ||
		  			this.bill.payment == '' ||
		  			this.bill.due_amount == '' ||
		  			this.bill.payment_method == ''
		  			) {
		  			swal("All fields are require. And atlease one sale item is require.");
		  			return 0;
		  		} else {
		  			var that1 = this;
		  			$.ajax({
		  			        type: "POST",
		  			        data: {
		  			        	bill: that.bill,
		  			        	purchase_list: that.purchase_list 
		  			        },
							url: "restcontroller/newsales.php",
		  			        success: function(data){
		  			           		swal(data);
		  			           		that1.filter.product = '',
		  			           		that1.filter.attribute_type = '',
		  			           		that1.filter.attribute = '',
		  			           		that1.filter.purchase = '',
		  			           		that1.filter.quantity = '',
		  			           		
		  			           		that1.bill.number= '',
		  			           		that1.bill.customer_id= '',
		  			           		that1.bill.date= '',
		  			           		that1.bill.total= 0,
		  			           		that1.bill.grandtotal= 0,
		  			           		that1.bill.payment= 0,
		  			           		that1.bill.due_amount= 0,
		  			           		that1.bill.payment_method= '',
		  			           		that1.bill.due_date= '',

		  			           		that1.purchase_list = []
		  			               },
		  			           error: function(error) {
		  			           		swal(error);
		  			           }
		  			    });
		  		}
		  	},

		  	deleteElement(index) {
		  		this.purchase_list.splice(index, 1);
		  	},

		  	fetch_products: function() {
		  		var that = this;
	  			$.ajax({
	  			           type: "GET",
	  			           url: "restcontroller/fetchProducts.php",
	  			           success: function(data){
	  			           		that.products = JSON.parse(data);
	  			               },
	  			           error: function(error) {
	  			           		swal(error);
	  			           }
	  			    });
	  			return 0;
		  	},

		  	fetch_attribute_types: function(product_id) {
		  		var that = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fetchProductAttributes.php?id="+product_id,
		  			success: function (data) {
		  				var data = JSON.parse(data);
		  				if(data.code === '404') {
		  					that.attribute_types = [];
		  					that.filter.attribute_type = '';
		  					swal(data.message);
		  				} else {
		  					that.attribute_types = [];
		  					that.attribute_types = data.data;
		  				}
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	},

		  	fetch_attributes: function(attribute_type, product_id) {
		  		var that = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fetchAttributes.php?attr_type="+attribute_type+"&prod_id="+product_id,
		  			success: function (data) {
		  				var mydata = JSON.parse(data);
		  				that.attributes = mydata.data;
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	},
//============================================================
//              fetch_printbill: function (filter) {
//		  		var that = this;
//		  		$.ajax({
//		  			type: "GET",
//		  			url: "restcontroller/newsales.php",
//		  			success: function (data) {
//		  				var mydata = JSON.parse(data);
//		  				that.prinbill = mydata;
//		  			},
//		  			error: function (error) {
//		  				console.log(error);
//		  			}
//		  		});
//		  	},
//============================================================
		  	fetch_purchases: function (filter) {
		  		var that = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fetchPurchase.php?product="+filter.product+"&attribute_type="+filter.attribute_type+"&attribute="+filter.attribute,
		  			success: function (data) {
		  				var mydata = JSON.parse(data);
		  				that.purchases = mydata;
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	},
		  	fetch_customers: function () {
		  		var that = this;
		  		$.ajax({
		  			type: "GET",
		  			url: "restcontroller/fatchCustomer.php",
		  			success: function (data) {
		  				var mydata = JSON.parse(data);
		  				that.customers = mydata;
		  			},
		  			error: function (error) {
		  				console.log(error);
		  			}
		  		});
		  	},

		  	addPurchase(purchase_id) {
		  		var that = this;
		  		if(this.filter.product == '') {
		  			swal("Requires all fields.");
		  			return 0;
		  		}
		  		if(this.filter.attribute_type == '') {
			  		swal("Requires all fields.");
			  		return 0;	
		  		}
		  		if(this.filter.attribute == '') {
		  			swal("Requires all fields.");
		  			return 0;
		  		}
		  		if(this.filter.purchase == '') {
		  			swal("Requires all fields.");
		  			return 0;
		  		}
		  		if(this.filter.quantity == '') {
		  			swal("Requires all fields.");
		  			return 0;
		  		}

		  		$.each(this.purchases, function(index, value) {
		  			if(value.purchase_id == purchase_id) {
		  				if(that.filter.quantity > value.quantity) {
		  					swal("You can purchase upto "+value.quantity+" units.");
		  					return 0;
		  				}
		  				if(that.filter.quantity < 1) {
		  					swal("Purchase quantity must be greater or equal to 1");
		  					return 0;
		  				}
		  				value.quantity = that.filter.quantity;
		  				that.filter.product_id = value.id;
		  				that.purchase_list.push(value);
		  				that.filter.product= '',
		  				that.filter.attribute_type= '',
		  				that.filter.attribute= '',
		  				that.filter.purchase= '',
		  				that.filter.quantity= '',
		  				that.filter.product_id = ''
		  			}
		  		});
		  	},

		  	calculatetotal() {
		  		this.bill.grandtotal = this.bill.total + this.bill.total * .14;
		  	},

		  	calculatedue() {
		  		if(this.bill.payment > this.bill.grandtotal) {
		  			this.bill.payment = this.bill.grandtotal.toFixed(2);
		  		}
		  		var due = this.bill.grandtotal - this.bill.payment;
		  		this.bill.due_amount = due.toFixed(2);
		  	}
		  },
 
		  mounted: function () {
		  	this.fetch_products();
		  	this.fetch_customers();
		  },

		  watch: {
		  	'filter.product': {
		  		handler: function () {
		  			var that = this;
		  			if(that.filter.product == "") {
		  				return 0;
		  			}
		  			this.fetch_attribute_types(this.filter.product);
		  		},
		  		deep: true
		  	},
		  	'filter.attribute_type': {
		  		handler: function () {
		  			var that = this;
		  			if(that.filter.attribute_type == "") {
		  				return 0;
		  			}
		  			this.fetch_attributes(this.filter.attribute_type, this.filter.product);
		  		},
		  		deep: true
		  	},
		  	'filter.attribute': {
		  		handler: function () {
		  			var that = this;
		  			if(that.filter.attribute == "") {
		  				return 0;
		  			}
		  			this.fetch_purchases(that.filter);
		  		},
		  		deep: true
		  	},
		  	'purchases': function () {
		  		var that = this;
		  		$.each(this.purchase_list, function (index, value) {
		  			$.each(that.purchases, function (i, val) {
		  				if(value.purchase_id == val.purchase_id) {
		  					var tmpQuantity = that.purchases[i].quantity;
		  					var lstQuantity = that.purchase_list[index].quantity;
		  					that.purchases[i].quantity = that.purchases[i].quantity - that.purchase_list[index].quantity;
		  					return 0;
		  				}
		  			});

		  		});
		  	},
		  	'purchase_list': function() {
		  		var tot = 0;
		  		$.each(this.purchase_list, function(index, value){
		  			tot = value.quantity * value.sales_rate + tot;
		  		});
		  		this.bill.total = tot;
		  	},

		  	'bill.total': function() {
		  		this.bill.total_after_tax = this.bill.total * .14 + this.bill.total;
		  	},
		  	'bill.total': function() {
		  		this.calculatetotal();
		  		this.calculatedue();
		  	},
		  	'bill.payment': function () {
		  		this.calculatedue();
		  	}
		  	// watch end
		  },

		  filters: {
		  	twodigitdecimal: function (value) {
		  		return parseFloat(value).toFixed(2);
		  	}
		  }

		  // vue end
		})
	</script>
</body>

</html>